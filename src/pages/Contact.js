import React from "react";

import { Grid, Typography, Container, Grow } from "@material-ui/core";
import ContactCard from "./../components/ContactCard";

const Contact = () => {
  return (
    <Container maxWidth="md">
      <Grid container spacing={3}>
        <Grid item md={4} sm={12} xs={12}>
          <ContactCard />
        </Grid>
        <Grid item md={8} sm={12} xs={12}>
          <Grow in={true}>
            <div>
              <Typography variant="h4" align="center">
                About Me
              </Typography>
              <Typography variant="body1">
                Hello My name is <b>Saikiran Rudra</b>, I am from{" "}
                <b>Gujarat, India</b>, currently pursuing my{" "}
                <b>Btech in Computer Science</b>{" "}
                <span role="img" aria-label="computer science">
                  👨‍💻
                </span>
                , at <b>Parul University</b>{" "}
                <span role="img" aria-label="graduation">
                  🎓
                </span>{" "}
                I am a <b>Full Stack Web developer</b>
                <span role="img" aria-label="web developer">
                  🌐
                </span>{" "}
                and{" "}
                <b>
                  Data Science
                  <span role="img" aria-label="data scientist">
                    📉
                  </span>
                  , Machine Learning
                  <span role="img" aria-label="machine learning">
                    🤖
                  </span>{" "}
                  Enthusist
                </b>
                . Passionate About Solving Real World Problems
              </Typography>
            </div>
          </Grow>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Contact;

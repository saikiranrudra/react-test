import React, { useState } from "react";
import Search from "./../components/Search";
import ProjectCard from "../components/ProjectCard";
import { Button, Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// images
import css1 from "./../assets/css1.jpg";

const useStyle = makeStyles((theme) => ({
  container: {
    display: "grid",
    gridTemplateColumns: "repeat(auto-fit, minmax(auto,320px))",
    gridGap: ".6rem",
    gridAutoFlow: "row dense",
    justifyContent: "center",
  },
  tagsContainer: {
    textAlign: "center",
  },
}));

const data = [
  {
    title: "Flex Box Project",
    content:
      "Entire project is build using css flex box which is responsive too. Build while learning taking css advance scass course",
    coverImg: css1,
    tags: ["html", "css", "flexbox", "scss"],
  },
  {
    title: "Tourism",
    content:
      "This is a tourism website were information about toursim is given",
    tags: ["html", "css", "javascript", "nodejs"],
    live: "#",
    github: "#",
  },
  {
    title: "Nature",
    content:
      "This is a random content for the project which explain about the project. this is s detail discription",
    tags: ["html", "css", "wordpress", "php"],
    live: "#",
  },
  {
    title: "Tribute",
    content:
      "This is a tribute website were information about tribute is given",
    tags: ["nextjs", "css", "react", "nodejs"],
    github: "#",
  },
  {
    title: "e commerse",
    content:
      "This is a tribute website were information about tribute is given",
    tags: ["metrojs", "css", "react", "nodejs"],
  },
];

let allTags = [];
data.forEach((item) => {
  allTags = [...allTags, ...item.tags];
});
allTags = allTags.filter((item, index) => index === allTags.indexOf(item));

const Projects = () => {
  const classes = useStyle();
  const [search, setSearch] = useState("");

  return (
    <>
      <Search search={search} setSearch={setSearch} />
      <Container maxWidth="xl" className={classes.tagsContainer}>
        <div style={{ margin: "1rem" }}>
          <Button
            size="small"
            key={-1}
            onClick={() => setSearch("")}
            color="secondary"
          >
            all
          </Button>
          {allTags.map((t, i) => (
            <Button
              size="small"
              key={i}
              onClick={() => setSearch(t)}
              color="secondary"
            >
              {t}
            </Button>
          ))}
        </div>
      </Container>
      <Container maxWidth="xl">
        <div className={classes.container}>
          {data.map((item, index) => {
            if (
              item.title.toLowerCase().includes(search.toLowerCase()) ||
              item.tags.includes(search)
            ) {
              return (
                <ProjectCard
                  key={index}
                  coverImg={
                    item.coverImg
                      ? item.coverImg
                      : "linear-gradient(to right, #b500f7, #6800ff)"
                  }
                  title={item.title}
                  content={item.content}
                  tags={item.tags}
                  live={item.live}
                  github={item.github}
                />
              );
            } else {
              return null;
            }
          })}
        </div>
      </Container>
    </>
  );
};

export default Projects;

import React from "react";

import { useMediaQuery } from "@material-ui/core";

import { Link } from "react-router-dom";
import Logo from "../Logo";
import { makeStyles } from "@material-ui/core/styles";

import MobileMenu from "./MobileMenu";

const useStyle = makeStyles({
  menu: {
    margin: "1rem",
    display: "flex",
    "&:hover > div > div": {
      width: "11rem",
      borderRadius: "4px",
      backgroundImage: "linear-gradient(to right,#A200FF, #FF00C4)",
      fontSize: "1rem",
    },
  },
  menuItems: {
    marginLeft: "auto",
    "& > span": {
      padding: ".8rem .4rem .4rem .4rem",
      margin: "0 .4rem",
      cursor: "pointer",
      borderBottom: "5px solid transparent",
      transition: "all .4s",
      "&:hover": {
        borderBottom: "3px solid #A200FF",
      },
    },
  },

  link: {
    cursor: "pointer",
    textDecoration: "none",
    color: "inherit",
  },
});

const MenuBar = () => {
  const classes = useStyle();
  const widthCheck = useMediaQuery("(max-width: 736px)");
  return (
    <>
      <div className={classes.menu}>
        <Logo name="Saikiran Rudra" />
        {widthCheck ? (
          <MobileMenu />
        ) : (
          <div className={classes.menuItems}>
            <span>RESUME</span>
            <span>
              <Link className={classes.link} to="/projects">
                PROJECTS
              </Link>
            </span>
            <span>
              <Link className={classes.link} to="/contact/about">
                CONTACT / ABOUT
              </Link>
            </span>
            <span>
              <a
                href="https://medium.com/@saikiranrudra"
                target="_blank"
                rel="noopener noreferrer"
                className={classes.link}
              >
                BLOGS
              </a>
            </span>
          </div>
        )}
      </div>
    </>
  );
};

export default MenuBar;

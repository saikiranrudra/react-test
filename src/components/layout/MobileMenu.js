import React, { useState } from "react";

import {
  IconButton,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";

import {
  Menu as MenuIcon,
  ChevronRight as ChevronRightIcon,
  AssignmentInd,
  AccountTree,
  Contacts,
  Web,
} from "@material-ui/icons";

import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles({
  mobileMenu: {
    marginLeft: "auto",
  },
  link: {
    cursor: "pointer",
    textDecoration: "none",
    color: "inherit",
  },
});

const MobileMenu = () => {
  const classes = useStyle();
  const [slide, setSlide] = useState(false);

  const toggleDrawer = (doState) => {
    setSlide(doState);
  };

  return (
    <div className={classes.mobileMenu}>
      <IconButton onClick={() => toggleDrawer(true)}>
        <MenuIcon />
      </IconButton>

      <Drawer anchor="right" open={slide} onClose={() => toggleDrawer(false)}>
        <div style={{ width: "85vw" }}>
          <IconButton onClick={() => toggleDrawer(false)}>
            <ChevronRightIcon />
          </IconButton>
        </div>

        <List>
          <ListItem button>
            <ListItemIcon>
              <AssignmentInd />
            </ListItemIcon>
            <ListItemText>RESUME</ListItemText>
          </ListItem>

          <ListItem button>
            <ListItemIcon>
              <AccountTree />
            </ListItemIcon>

            <ListItemText onClick={() => toggleDrawer(false)}>
              <Link className={classes.link} to="/projects">
                PROJECTS
              </Link>
            </ListItemText>
          </ListItem>

          <ListItem button>
            <ListItemIcon>
              <Contacts />
            </ListItemIcon>

            <ListItemText onClick={() => toggleDrawer(false)}>
              <Link className={classes.link} to="/contact/about">
                CONTACT/ABOUT
              </Link>
            </ListItemText>
          </ListItem>

          <ListItem button>
            <ListItemIcon>
              <Web />
            </ListItemIcon>

            <ListItemText>
              <a
                className={classes.link}
                href="https://medium.com/@saikiranrudra"
                target="_blank"
                rel="noopener noreferrer"
              >
                BLOGS
              </a>
            </ListItemText>
          </ListItem>
        </List>
      </Drawer>
    </div>
  );
};

export default MobileMenu;

import React from "react";

// components
import MenuBar from "./layout/MenuBar";
import HeroText from "./HeroText";

//pages
import Contact from "./../pages/Contact";
import Projects from "./../pages/Projects";
import { BrowserRouter, Route } from "react-router-dom";

const App = () => {
  return (
    <BrowserRouter>
      <Route path="/">
        <MenuBar />
      </Route>
      <Route path="/" exact>
        <HeroText />
      </Route>
      <Route path="/contact/about" exact>
        <Contact />
      </Route>
      <Route path="/projects" exact>
        <Projects />
      </Route>
    </BrowserRouter>
  );
};

export default App;

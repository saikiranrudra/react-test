import React from "react";

import {
  Paper,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Grow,
} from "@material-ui/core";
import {
  AcUnit,
  AlternateEmail,
  LinkedIn,
  Phone,
  WhatsApp,
  GitHub,
} from "@material-ui/icons";

const ContactCard = () => {
  return (
    <Grow in={true}>
      <Paper>
        <List>
          <ListItem button>
            <ListItemIcon>
              <AcUnit />
            </ListItemIcon>

            <ListItemText>Saikiran Rudra</ListItemText>
          </ListItem>

          <ListItem button>
            <ListItemIcon>
              <AlternateEmail />
            </ListItemIcon>

            <ListItemText>saikiranrudra2@gmail.com</ListItemText>
          </ListItem>

          <a
            href="https://in.linkedin.com/in/saikiranrudra"
            rel="noopener noreferrer"
            target="_blank"
            style={{ textDecoration: "none", color: "inherit" }}
          >
            <ListItem button>
              <ListItemIcon>
                <LinkedIn />
              </ListItemIcon>

              <ListItemText>/in/saikiranrudra</ListItemText>
            </ListItem>
          </a>

          <a
            href="github.com/saikiranrudra/"
            rel="noopener noreferrer"
            target="_blank"
            style={{ textDecoration: "none", color: "inherit" }}
          >
            <ListItem button>
              <ListItemIcon>
                <GitHub />
              </ListItemIcon>

              <ListItemText>/saikiranrudra</ListItemText>
            </ListItem>
          </a>

          <ListItem button>
            <ListItemIcon>
              <Phone />
            </ListItemIcon>

            <ListItemText>8160921794</ListItemText>
          </ListItem>

          <ListItem button>
            <ListItemIcon>
              <WhatsApp />
            </ListItemIcon>

            <ListItemText>9924583549</ListItemText>
          </ListItem>
        </List>
      </Paper>
    </Grow>
  );
};

export default ContactCard;

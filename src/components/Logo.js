import React from "react";

import { makeStyles } from '@material-ui/core/styles';
import {Link} from "react-router-dom";

const useStyle = makeStyles({
    logo: {
        fontSize: "1.6rem",
        fontWeight: "700",
        display: "flex",
        width: "18rem",
        marginLeft: ".3rem",
        "& > div": {
            transition: "all .3s",
            width: ".3rem",
            height: "2rem",
            marginLeft: ".3rem",
            backgroundImage: "linear-gradient(to bottom,#A200FF, #FF00C4)",
            borderRadius: "1rem", 
            fontSize: 0,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            color: "#eee"
        }        
    }
})

const Logo = props => {
    const classes = useStyle();
    return (
        <div className={classes.logo}>
            <span>
                <Link 
                    to="/" 
                    className={classes.link}
                    style={{
                        textDecoration: "none",
                        color: "inherit"
                    }}
                >
                    SR
                </Link>
            </span>
            <div>{props.name}</div>
        </div>
    );
}

export default Logo;
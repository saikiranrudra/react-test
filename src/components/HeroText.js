import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import { useMediaQuery, Fade } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  container: {
    position: "absolute",
    display: "inline-block",
    top: "50%",
    left: "50%",
    fontWeight: 700,
    width: "40.4rem",
    transform: "translate(-50%, -50%)",
    [theme.breakpoints.down("md")]: {
      width: "32.4rem",
    },
    [theme.breakpoints.down("sm")]: {
      width: "20.32rem",
    },
    [theme.breakpoints.down("xs")]: {
      width: "20.32rem",
    },
  },
  big: {
    fontSize: "5rem",
    [theme.breakpoints.down("md")]: {
      fontSize: "4rem",
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "2.5rem",
    },
  },
}));

const HeroText = () => {
  const classes = useStyles();
  const widthCheck = useMediaQuery("(max-width: 360px)");
  return (
    <Fade in={true}>
      <div
        className={classes.container}
        style={{
          width: widthCheck ? "19.5rem" : null,
          transform: widthCheck ? "translate(-50%, -50%) scale(.8)" : null,
        }}
      >
        <div>I'M</div>
        <span
          className={classes.big}
          style={{
            fontSize: widthCheck ? "2.4rem" : null,
          }}
        >
          SAIKIRAN RUDRA
        </span>
        <div style={{ textAlign: "right" }}>
          Full Stack Developer and ML DS Enthusist
        </div>
      </div>
    </Fade>
  );
};

export default HeroText;

import React from "react";
import {
  Card,
  CardMedia,
  CardContent,
  Typography,
  Button,
  Grow,
} from "@material-ui/core";

import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles((theme) => ({
  card: {
    maxWidth: "20rem",
  },
  media: {
    height: 200,
  },
}));

const renderActionBtn = (props) => {
  if (props.live && props.github) {
    return (
      <>
        <Button
          variant="contained"
          size="small"
          color="primary"
          style={{ marginRight: ".3rem" }}
        >
          Live
        </Button>
        <Button
          variant="contained"
          size="small"
          color="secondary"
          style={{ marginLeft: ".3rem" }}
        >
          GitHub
        </Button>
      </>
    );
  } else if (props.live) {
    return (
      <Button
        variant="contained"
        size="small"
        color="primary"
        style={{ marginRight: ".3rem" }}
      >
        Live
      </Button>
    );
  } else if (props.github) {
    return (
      <Button
        variant="contained"
        size="small"
        color="secondary"
        style={{ marginLeft: ".3rem" }}
      >
        GitHub
      </Button>
    );
  } else {
    return (
      <Button
        variant="contained"
        size="small"
        color="primary"
        disabled
        style={{ marginLeft: ".3rem" }}
      >
        No Links Available
      </Button>
    );
  }
};

const trimContent = (sentence) => {
  if (sentence.length > 80) {
    return sentence.slice(0, 80) + "...";
  } else {
    return sentence;
  }
};

const ProjectCard = (props) => {
  const classes = useStyle();
  return (
    <Grow in={true}>
      <Card className={classes.card}>
        <CardMedia
          title={props.title}
          className={classes.media}
          image="#"
          style={{
            backgroundImage: props.coverImg.startsWith("/")
              ? `url(${props.coverImg})`
              : props.coverImg,
          }}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {props.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {trimContent(props.content)}
          </Typography>

          <div>
            <h3>Tags</h3>
            {props.tags.map((tag, index) => (
              <Button size="small" key={index} color="secondary">
                {tag}
              </Button>
            ))}
          </div>
          <div style={{ display: "flex" }}>
            <div style={{ marginLeft: "auto", display: "inline-block" }}>
              {renderActionBtn(props)}
            </div>
          </div>
        </CardContent>
      </Card>
    </Grow>
  );
};

// ProjectCard.defaultProps = {
//   coverImg: "linear-gradient(to right, #b500f7, #6800ff)",
//   title: "The Project Title",
//   content:
//     "This is a random content for the project which explain about the project. this is s detail discription",
//   tags: ["html", "css", "javascript", "nodejs"],
// };

export default ProjectCard;

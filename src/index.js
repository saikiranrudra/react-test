import React, { Suspense } from "react";
import ReactDOM from "react-dom";

import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import App from "./components/App";

const darkTheme = createMuiTheme({
  palette: {
    type: "dark",
  },
});

ReactDOM.render(
  <ThemeProvider theme={darkTheme}>
    <CssBaseline />
    <Suspense fallback={<h1>App is Loading...</h1>}>
      <App />
    </Suspense>
  </ThemeProvider>,
  document.getElementById("root")
);
